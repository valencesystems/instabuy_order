require('dotenv').config()
const express = require('express')
const app = express()
var cors = require('cors')
var bodyParser = require('body-parser');
var axios = require('axios')
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

var corsOrigin = 'https://www.thingisaur.com'
if(process.env.DEVMODE == true){
  corsOrigin = 'http://localhost:3000'
}

var corsOptions = {
  origin: corsOrigin,
  optionsSuccessStatus: 204 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

//Mongo
var db;
const url = process.env.MONGODB_URI;
const dbName = process.env.DBNAME;

//Stripe
var stripeKey;
var customCatSandbox;
if(process.env.GOLIVE == true){
  stripeKey = process.env.STRIPE_API_LIVE_KEY;
  customCatSandbox = 0;
}
else{
  stripeKey = process.env.STRIPE_API_TEST_KEY;
  customCatSandbox = 1;
}
var stripe = require("stripe")(stripeKey);

const sgMail = require('@sendgrid/mail');

var key = process.env.SENDGRID_API_KEY
sgMail.setApiKey(key);

app.post('https://customcat-beta.mylocker.net/api/v1/webhook', function(request, response) {
  // Retrieve the request's body and parse it as JSON:
  const event_json = JSON.parse(request.body);

  const msg = {
    to: 'logins@auricom.com',
    from: 'support@thingisaur.com',
    subject: 'order shipped',
    text: event_json,
    html: ''
  }

  sgMail.send(msg);

  response.send(200);
});

app.get('/', function (req, res) {
  res.redirect('https://www.thingisaur.com');
});

// POST method route

function findProductbySku(db, stripeSku, callback) {
  // Get the documents collection
  const collection = db.collection('products');
  // Find some documents
  collection.find({'stripeSku': stripeSku}).toArray(function(err, docs) {
    assert.equal(err, null);
    callback(docs);
  });
}

app.post('/productInfo', function (req, res) {

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    db = client.db(dbName);
    
    findProductbySku(db, req.body.stripeSku, function(docs){
      res.send(docs[0]);
      client.close();
    })
  });
})

// POST method route
app.post('/submitorder', function (req, res) {

  var product;
  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    db = client.db(dbName);
    
    findProductbySku(db, req.body.stripeSku, function(docs){
      product = docs[0];
      client.close();
    })
  });
  var order_id;
  stripe.orders.create({
      currency: 'usd',
      items: [
        {
          type: 'sku',
          parent: req.body.stripeSku
        }
      ],
      shipping: {
        name: req.body.formdata.stripeBillingName,
        address: {
          line1: req.body.formdata.stripeShippingAddressLine1,
          city: req.body.formdata.stripeShippingAddressCity,
          state: req.body.formdata.stripeShippingAddressState,
          country: req.body.formdata.stripeShippingAddressCountry,
          postal_code: req.body.formdata.stripeShippingAddressZip
        }
      },
      email: req.body.formdata.stripeEmail
    }, function(err, order) {
      // asynchronously called
      
      if(order){

        order_id = order.id.slice(3)

        stripe.charges.create({
            amount: product.ammount,
            currency: "usd",
            source: req.body.formdata.stripeToken, // obtained with Stripe.js
            description: "Charge for" + product.productName,
          }, function(err, charge) {
            // asynchronously called
            if(charge){
              const msg = {
                template_id: "86d7f38d-f85b-421a-9761-44477b0f6b57",
                "substitutions": {
                  productName: product.productName,
                  cost: product.buttonAmmount,
                  shippingAmmount: product.shippingAmmount,
                  orderid: order_id,
                  dname: req.body.formdata.stripeShippingName,
                  dline: req.body.formdata.stripeShippingAddressLine1,
                  dline2: req.body.formdata.stripeShippingAddressCity + ", " + req.body.formdata.stripeShippingAddressState,
                  dline3: req.body.formdata.stripeShippingAddressCountry + ", " + req.body.formdata.stripeShippingAddressZip,
                  bname: req.body.formdata.stripeBillingName,
                  bline: req.body.formdata.stripeBillingAddressLine1,
                  bline2: req.body.formdata.stripeBillingAddressCity + ", " + req.body.formdata.stripeBillingAddressState,
                  bline3: req.body.formdata.stripeBillingAddressCountry + ", " + req.body.formdata.stripeBillingAddressZip,
                },
                to: req.body.formdata.stripeEmail,
                from: 'support@thingisaur.com',
                subject: 'Order Confirmation',
              }
              sgMail.send(msg).then(() => {
                axios.post('https://customcat-beta.mylocker.net/api/v1/order/' + order_id, {
                  "shipping_first_name": req.body.formdata.stripeShippingName,
                  "shipping_last_name": req.body.formdata.stripeShippingName,
                  "shipping_address1": req.body.formdata.stripeShippingAddressLine1,
                  "shipping_address2": "",
                  "shipping_city": req.body.formdata.stripeShippingAddressCity,
                  "shipping_state": req.body.formdata.stripeShippingAddressState,
                  "shipping_zip": req.body.formdata.stripeShippingAddressZip,
                  "shipping_country": "US",
                  "shipping_email": req.body.formdata.stripeEmail,
                  "shipping_phone": "",
                  "shipping_method": "Economy",
                  "items": [
                    {
                      "sku": product.customCatSku,
                      "quantity": 1
                    }
                  ],
                  "sandbox": customCatSandbox,
                  "api_key": process.env.CUSTOMCAT_API_KEY
                })
                .then(function (response) {
                  res.send(order_id)
                })
                .catch(function (error) {
                            console.log(error.response.data.ERROR)
                });
            }); 
            }
          });
        }   
    });
})

  var port = process.env.PORT || 4000
  app.listen(port, () => console.log('Example app listening on port 4000!'))